# WEBIMOINE

## Comment lancer l'application

À la première utilisation, les modules Node doivent être initialisés avec la commande " npm install " exécutée à la racine du dossier /src/main/js/patrimoine/

* Démarrer les services MySQL est associés (WAMP / LAMP)
* Lancer l'application Spring (/src/main/java/pam/Application)
* Se placer à la racine du dossier /src/main/js/patrimoine/ et exécuter la commande " ng serve --open " (ou juste " ng serve " et se connecter à l'adresse http://localhost:4200)

À noter que l'application est donc manipulé depuis le serveur Angular (http://localhost:4200), le serveur Spring n'est qu'utilisé par Angular (http://locahost:8080) en tant qu'API REST.
