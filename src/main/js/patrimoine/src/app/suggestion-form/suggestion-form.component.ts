import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { AddMonumentService } from '../add-monument.service';

import { switchMap } from 'rxjs/operators';
import {ConnectionService} from "../connection.service";

declare var $ : any;

const MIN_NAME_LEN = 3;
const MIN_DESC_LEN = 5;

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './suggestion-form.component.html',
  styleUrls: ['./suggestion-form.component.css']
})
export class SuggestionFormComponent implements OnInit {
    title = "Suggestion de nouveau monument";

    submitTitle="Succès";
    submitMsgs=[];

    name = "";
    description ="";
    type = "";
    monumentCategory = [
            {id: 'historical', name: 'Monument Historique'},
            {id: 'house', name: 'Maisons Natales'},
            {id: 'museum', name: 'Musées'},
            {id: 'sculpture', name: 'Sculptures'},
            {id: 'religious', name: 'Lieux De Culte'},
            {id: 'mural', name: 'Murales'},
            {id: 'anecdote', name: 'Anecdotes'},
    ];
    longitude = "0";
    latitude = "0";

    constructor(
        private addMonumentService : AddMonumentService,
        private route : ActivatedRoute,
        private connectionService : ConnectionService,
        private router : Router
    ) { }

     ngOnInit() {
         let connecte = this.connectionService.isConnected();
         if (connecte) {
             this.longitude = this.route.snapshot.queryParamMap.get("longitude");
             this.latitude = this.route.snapshot.queryParamMap.get("latitude");
         } else {
             this.accesInterdit();
         }
     }

    accesInterdit() {
        this.submitTitle = "Accès interdit";
        this.submitMsgs.push("Vous ne pouvez pas accèder à cette page !");
        console.log(this.submitTitle);
        $("#suggestionAdded").modal('show');
    }

    closeFct(){
        if (!this.connectionService.isConnected()) {
            this.router.navigate(["/"]);
        }
    }

     onSubmit() {
        // On retire tous les messages d'informations liés à d'éventuelles précèdentes soumissions.
        this.submitMsgs = [];
        let error = false;

        if (this.name.length < MIN_NAME_LEN) {
            error = true;
            this.submitMsgs.push("Le nom doit être composée d'au moins " + MIN_NAME_LEN + " caractères.");
        }
        if (this.description.length < MIN_DESC_LEN) {
            error = true;
            this.submitMsgs.push( "La description doit être composée d'au moins " + MIN_DESC_LEN + " caractères.");
        }
        if (this.type == "") {
            error = true;
            this.submitMsgs.push("Vous devez renseigner une catégorie.");
        }

        if (error) {
            this.submitTitle = "Erreur";
            $("#suggestionForm").addClass("was-validated");
        } else {
            this.submitTitle = "Succès";
            $("#suggestionForm").removeClass("was-validated");
            console.log("Type du monument ajoute : ", this.type);
            this.addMonumentService.addMonument(this.name, this.description, this.type, this.latitude, this.longitude);
            this.name = "";
            this.description = "";
            this.type = "";
            this.submitMsgs.push("La suggestion a été ajoutée");
        }
        $("#suggestionAdded").modal('show');
     }
}
