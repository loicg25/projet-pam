import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import TileLayer from 'ol/layer/Tile.js';
import Vector from 'ol/layer/Vector';
import OSM from 'ol/source/OSM.js';
import Feature from 'ol/Feature.js';
import * as style from 'ol/style';
import * as proj from 'ol/proj';
import * as source from 'ol/source';
import * as geom from 'ol/geom';
import Overlay from 'ol/Overlay';

import { MonumentGetterService } from '../monument-getter.service';
import { Monument } from '../monument';
import {ConnectionService} from "../connection.service";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
    title = 'Patrimoine';

    latitude: number = -1;
    longitude: number = -1;
    map: any;
    clickedLonlat = null;

    selectedMonument = {
        id: -1
    };

    colorMapping = {
        "historical": "#ffbf00",
        "house": "#0362fc",
        "museum": "#03c200",
        "sculpture": "#000000",
        "religious": "#0d00ff",
        "anecdote": "#f200ff",
        "mural": "#00ff73"
    }

    connected = false;

    constructor(
        private monumentGetter: MonumentGetterService,
        private connectionService : ConnectionService,
    ) {}

    ngOnInit() {
        this.connected = this.connectionService.isConnected();
        this.getPosition();
    }

    handlePopUp() {
        /**
         * Elements that make up the popup.
         */
        const container = document.getElementById('popup');
        container.style.display = "block";
        const closer = document.getElementById('popup-closer');

        /**
         * Create an overlay to anchor the popup to the map.
         */
        const overlay = new Overlay({
            element: container,
            autoPan: true,
            autoPanAnimation: {
                duration: 250,
            },
        });

        /**
         * Add a click handler to hide the popup.
         * @return {boolean} Don't follow the href.
         */
        closer.onclick = function () {
            overlay.setPosition(undefined);
            closer.blur();
            return false;
        };

        return overlay;
    }

    computeMap() {
      const overlay = this.handlePopUp();
      this.displayMap(overlay);
      /*for (var monumentType in this.colorMapping) {
        if (this.filters[monumentType] == 1) {
            this.monumentGetter.getMonumentsByType(monumentType).subscribe(monuments => this.addMonuments(monuments, monumentType));
        }
      }*///doesn't work due to asynchronicity on monumentType

      this.monumentGetter.getMonumentsByType("historical").subscribe(monuments => this.addMonuments(monuments, "historical"));
      this.monumentGetter.getMonumentsByType("house").subscribe(monuments => this.addMonuments(monuments, "house"));
      this.monumentGetter.getMonumentsByType("museum").subscribe(monuments => this.addMonuments(monuments, "museum"));
      this.monumentGetter.getMonumentsByType("sculpture").subscribe(monuments => this.addMonuments(monuments, "sculpture"));
      this.monumentGetter.getMonumentsByType("religious").subscribe(monuments => this.addMonuments(monuments, "religious"));
      this.monumentGetter.getMonumentsByType("anecdote").subscribe(monuments => this.addMonuments(monuments, "anecdote"));
      this.monumentGetter.getMonumentsByType("mural").subscribe(monuments => this.addMonuments(monuments, "mural"));
    }

    displayMap(overlay){
        this.map = new Map({
            target: 'map',
            layers: [
                new TileLayer({
                source: new OSM()
                })
            ],
            overlays: [overlay],
            view: new View({
                center: proj.fromLonLat([this.longitude, this.latitude]),
                zoom: 12
            })
        });
        this.addPoint(this.latitude, this.longitude);
        this.map.on("click", e => {
            this.clickedLonlat = proj.transform(e.coordinate, 'EPSG:3857', 'EPSG:4326');
            console.log(this.clickedLonlat)
            this.selectedMonument = {
                id: -1
            };
            overlay.setPosition(e.coordinate);
            let number = 0;
            this.map.getLayers().forEach(layer => {
                 this.map.forEachFeatureAtPixel(e.pixel,(feature, layer) => {
                    if (feature.values_.name !== "you") {
                        this.selectedMonument = feature.values_;
                        number++;
                    }
                });
            });
        });
    }

    getPosition(){
        if (navigator.geolocation){
          navigator.geolocation.getCurrentPosition(position=>{
              this.latitude = position.coords.latitude;
              this.longitude = position.coords.longitude;
              this.computeMap();
          }, err=>{
              this.computeMap();
          });
        } else {
        alert("Geolocation is not supported by this browser");
        }
    }

    addPoint(latitude, longitude){
      const layer = new Vector({
        source: new source.Vector({
          features: [
            new Feature({
              geometry: new geom.Point(proj.fromLonLat([longitude, latitude])),
              name: "you"
            })
          ],
        })
      });
      this.map.addLayer(layer);
    }

    addMonuments(monuments, type) {
        var layer = new Vector({
            source: new source.Vector({

            }),
            style: new style.Style({
                image: new style.Circle({
                  radius: 7,
                  fill: new style.Fill({
                    color: this.colorMapping[type]
                  })
                }),
                fill: new style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                })
            })
            });

        for (var monument of monuments) {
            if (monument.estValide == 1) {
                layer.getSource().addFeature(new Feature({
                    id: monument.id,
                    name: monument.nom,
                    geometry: new geom.Point(proj.fromLonLat([monument.longitude,monument.latitude]))
                }));
            }
        }
        this.map.addLayer(layer);
    }
}
