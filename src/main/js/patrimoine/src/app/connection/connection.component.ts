import { Component, OnInit } from '@angular/core';

import * as bcrypt from 'bcryptjs';
import { ConnectionService } from '../connection.service'

import { Utilisateur } from '../utilisateur';
import {Router} from "@angular/router";

declare var $ : any;
const MIN_PASS_LEN = 6;
const MIN_NAME_LEN = 3;

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {
    title="Se connecter";
    utilisateur: Utilisateur;
    submitTitle="Succès";
    submitMsgs=[];

    email="";
    password="";

  constructor(
    private connectionService : ConnectionService,
    private router : Router,
  ) { }

  ngOnInit() { }


  onSubmit() {
      // On retire tous les messages d'informations liés à d'éventuelles précèdentes soumissions.
      this.submitMsgs = [];
      let error = false;

      if (this.email == "" || this.password == "") {
          error = true;
          this.submitMsgs.push("Tous les champs doivent être remplis.");
      }

      if (error) {
          this.submitTitle = "Erreur";
          $("#connectionForm").addClass("was-validated");
      } else {
          this.connectionService.connection(this.email).subscribe(utilisateur => {
              this.utilisateur = utilisateur;
              if (this.utilisateur == undefined) {
                  this.submitTitle = "Erreur";
                  this.submitMsgs.push("Mot de passe incorrect ou utilisateur non inscrit.");
              } else {
                  const match = bcrypt.compareSync(this.password, this.utilisateur.mdp);
                  if (match) {
                      this.submitTitle = "Succès";
                      $("#connectionForm").removeClass("was-validated");
                      this.submitMsgs.push("Connecté ! Vous allez être redirigé...");
                      this.connectionService.authenticate(this.utilisateur.id_user, this.utilisateur.role);
                  } else {
                      this.submitTitle = "Erreur";
                      this.submitMsgs.push("Mot de passe incorrect ou utilisateur non inscrit.");
                  }
              }
              this.password = "";
              $("#myModal").modal('show');
          });
      }
  }

    closeFct(){
        if (this.submitTitle == "Succès") {
            this.router.navigate(["/"]);
        }
    }

}
