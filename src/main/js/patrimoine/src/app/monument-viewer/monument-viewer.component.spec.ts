import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonumentViewerComponent } from './monument-viewer.component';

describe('MonumentViewerComponent', () => {
  let component: MonumentViewerComponent;
  let fixture: ComponentFixture<MonumentViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonumentViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonumentViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
