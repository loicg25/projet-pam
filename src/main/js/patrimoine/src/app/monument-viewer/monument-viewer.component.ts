import { Component, OnChanges, Input } from '@angular/core';

import { Monument } from '../monument';

import { MonumentGetterService } from '../monument-getter.service';

import { ConnectionService } from '../connection.service';

import { UserService } from '../user.service';

@Component({
  selector: 'app-monument-viewer',
  templateUrl: './monument-viewer.component.html',
  styleUrls: ['./monument-viewer.component.css']
})
export class MonumentViewerComponent implements OnChanges {
  monument: Monument;
  @Input() id: number;
  distance: number;
  distanceAdresse: number;
  isVisibleDistanceAdresse = false;
  isVisibleDistancePosition = false;
  connected = false;

  constructor(
    private monumentGetter: MonumentGetterService,
    private connectionService: ConnectionService,
    private userService: UserService,
  ) { }

  ngOnChanges() {
    this.getMonument();
  }

  getMonument() {
    this.monumentGetter.getMonument(this.id).subscribe(monument => {
        this.monument = monument;

        this.getPosition();

        this.connected = this.connectionService.isConnected();
        if (this.connected) {
            this.userService.getUserById(this.connectionService.getId()).subscribe(user => {
                $.get("https://nominatim.openstreetmap.org/search?format=json&q="+user.adresse, coordUser => this.displayDistance(monument.latitude, monument.longitude, coordUser));
            });
        }
    });
  }

  displayDistance(lat, lon, coordUser){
     this.isVisibleDistanceAdresse = true;
     let d = this.getDistance(lat, lon, coordUser[0].lat, coordUser[0].lon);
     this.distanceAdresse = Math.round(d*100)/100;
  }

    degres2radian(x){
      return x * (Math.PI/180);
    }

    getDistance(lat1, lon1, lat2, lon2){
      //console.log("DISTANCE : ", lat1, ", ", lon1, " /// ", lat2, ", ", lon2);
      let earth_radius = 6378.137;
      let dLat = this.degres2radian(lat2-lat1);
      let dLon = this.degres2radian(lon2-lon1);
      let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.cos(this.degres2radian(lat1)) * Math.cos(this.degres2radian(lat2)) *
              Math.sin(dLon/2) * Math.sin(dLon/2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = earth_radius * c;
      return d;
    }

    getPosition(){
        if (navigator.geolocation){
          navigator.geolocation.getCurrentPosition(position=>{
              //this.latitude = position.coords.latitude;
              //this.longitude = position.coords.longitude;
              let d = this.getDistance(position.coords.latitude, position.coords.longitude, this.monument.latitude, this.monument.longitude);
              this.distance = Math.round(d*100)/100;
              this.isVisibleDistancePosition = true;
          }, err=>{
                console.log("Erreur lors de la récupération de la position actuelle : ", err);
          });
        } else {
            console.log("Le navigateur ne prend pas en charge la localisation");
        }
    }

}
