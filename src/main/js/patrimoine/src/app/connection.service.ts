import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Utilisateur } from './utilisateur';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
    authenticated = false;
    role;
    id;

  constructor(private http: HttpClient) { }

  connection(email: String): Observable<Utilisateur>{
    return this.http.get<Utilisateur>("http://localhost:8080/user/tryconnection?mail="+email);
  }

  authenticate(id: number, role: String) {
      this.role = role;
      this.id = id;
      this.authenticated = true;
  }

  disconnect() {
      this.role = "";
      this.id = null;
      this.authenticated = false;
  }

  isConnected(){
    return this.authenticated;
  }

  getRole(){
    return this.role;
  }

  getId(){
    return this.id;
  }
}
