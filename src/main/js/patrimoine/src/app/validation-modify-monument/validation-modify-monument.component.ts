import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { MonumentGetterService } from '../monument-getter.service';

import { AddMonumentService } from '../add-monument.service';

import { EventManager } from '@angular/platform-browser';

import { ModificationGetterService } from '../modification-getter.service';

import { ConnectionService } from '../connection.service';

import { UserService } from '../user.service';

declare var $ : any;

@Component({
  selector: 'app-validation-modify-monument',
  templateUrl: './validation-modify-monument.component.html',
  styleUrls: ['./validation-modify-monument.component.css']
})
export class ValidationModifyMonumentComponent implements OnInit {

    title="Validation des modifications";
    submitTitle="";
    submitMsgs=[];

    idModif : number;
    idMonument : number;

    listModifications = [];

    isAdmin = false;
    role = "";
    id_userCo : number;

    haveModifications = false;

    isVisibleDivListeModification = true;
    isVisibleFormValidation = false;

    monumentOriginalStyleCSS = "2px solid black";

    name : String = "";
    description : String ="";
    type : String = "";
    latitude : String = "";
    longitude : String = "";
    monumentCategory = [
            {id: 'historical', name: 'Monument Historique'},
            {id: 'house', name: 'Maisons Natales'},
            {id: 'museum', name: 'Musées'},
            {id: 'sculpture', name: 'Sculptures'},
            {id: 'religious', name: 'Lieux De Culte'},
            {id: 'anecdote', name: 'Murales'},
            {id: 'mural', name: 'Anecdotes'},
    ];

    monumentOriginal;

  constructor(
       private monumentGetter : MonumentGetterService,
       private addMonument : AddMonumentService,
       private router : Router,
       private eventManager: EventManager,
       private modificationGetter: ModificationGetterService,
       private connectionService : ConnectionService,
       private userService : UserService,
  ) {}

  ngOnInit() {

        let connecte = this.connectionService.isConnected();
        //console.log("Connecté ? ", test);
        if (connecte){
            this.role = this.connectionService.getRole();
            this.id_userCo = this.connectionService.getId();
            if (this.role == "Administrateur" || this.role == "Moderateur"){
                this.isAdmin = true;
                this.userService.getUserById(this.id_userCo).subscribe(user => {
                    $.get("https://nominatim.openstreetmap.org/search?format=json&q="+user.adresse, coordUser => this.getModifications(coordUser));
                });
            } else {
                this.accesInterdit();
            }
        } else {
            this.accesInterdit();
        }
        $("#form-validation").modal("none");
      }


  accesInterdit(){
        this.submitTitle = "Accès interdit";
        this.submitMsgs.push("Vous ne pouvez pas accèder à cette page !");
        $("#validationDialog").modal('show');
  }

    closeFct(){
      if (!this.isAdmin){
          this.router.navigate(["/"]); //maybe a remodifier
      } else if (!this.haveModifications){
          this.router.navigate(["/"]);
      } else {
          if (this.submitTitle == "Succès") {
              this.isVisibleDivListeModification = true;
              this.isVisibleFormValidation = false;
          }
      }
    }

    removeFromList(id) {
        let index = 0;
        for (let i = 0; i < this.listModifications.length; ++i) {
            if (this.listModifications[i].id == id) {
                index = i;
                break;
            }
        }
        this.listModifications.splice(index, 1);
    }

  onSubmit(){
    this.submitMsgs = [];
    let error = false;
    if (this.name == "" || this.description == "" || this.latitude == "" || this.longitude == "" || this.type == ""){
        error = true;
        this.submitMsgs.push("Tout les champs doivent être remplis");
    }
    if (error) {
        this.submitTitle = "Erreur";
        $("#suggestionForm").addClass("was-validated");
    } else {
        this.submitTitle = "Succès";
        $("#suggestionForm").removeClass("was-validated");
        this.addMonument.modifyMonument(this.idMonument, this.name, this.description, this.type, this.latitude, this.longitude, this.idModif);
        this.removeFromList(this.idModif);
        //this.addMonument.validateMonument(this.idM, this.name, this.description, this.type, this.latitude, this.longitude);
        this.submitMsgs.push("Le monument a bien été modifié !");
    }
    $("#validationDialog").modal('show');
  }

    getStyleCSSWithColor(typeBDD){
      if (typeBDD == "MonumentHistorique"){
              return 'solid 2px #ffbf00';
          }
          if (typeBDD == "MaisonNatale"){
              return 'solid 2px #0362fc';
          }
          if (typeBDD == "Musee"){
              return 'solid 2px #03c200';
          }
          if (typeBDD == "Sculture"){
              return 'solid 2px #000000';
          }
          if (typeBDD == "LieuxDeCulte"){
              return 'solid 2px #0d00ff';
          }
          if (typeBDD == "Murale"){
              return 'solid 2px #00ff73';
          }
          if (typeBDD == "Anecdote"){
              return 'solid 2px #f200ff';
          }
          return "";
        }

    getModifications(coordUser){
        this.modificationGetter.getModifications().subscribe(modifications => {
            let count = 0;
                for (let m in modifications){
                    let distance = this.getDistance(modifications[m].latitude, modifications[m].longitude, coordUser[0].lat, coordUser[0].lon);
                    //console.log("Distance entre monument ", modifications[m].id_modification, " et adresse : ", distance, " km");
                    if ((this.role == "Moderateur" && distance <= 50) || this.role == "Administrateur"){
                        let mon = modifications[m];
                        mon["styleCSS"] = this.getStyleCSSWithColor(modifications[m].type);
                        this.listModifications.push(mon);
                        count++;
                    }
                }
                if (count == 0){
                    this.submitTitle = "Info";
                    this.submitMsgs.push("Il n'y a pas de modification à valider");
                    $("#validationDialog").modal('show');
                } else {
                    this.haveModifications = true;
                }
        });
    }

      degres2radian(x){
        return x * (Math.PI/180);
      }

      getDistance(lat1, lon1, lat2, lon2){
        //console.log("DISTANCE : ", lat1, ", ", lon1, " /// ", lat2, ", ", lon2);
        let earth_radius = 6378.137;
        let dLat = this.degres2radian(lat2-lat1);
        let dLon = this.degres2radian(lon2-lon1);
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(this.degres2radian(lat1)) * Math.cos(this.degres2radian(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = earth_radius * c;
        return d;
      }

    choiceModification(id){
        this.affichageFormulaire(id);
    }

    affichageFormulaire(id){
        this.isVisibleDivListeModification = false;
        this.isVisibleFormValidation = true;

        //let id_monument = -1;

        for (let m in this.listModifications){
            if (this.listModifications[m].id_modification == id){
                this.idModif = this.listModifications[m].id_modification;
                this.name = this.listModifications[m].nom;
                this.description = this.listModifications[m].description;
                this.type = this.getType(this.listModifications[m].type);
                this.latitude = this.listModifications[m].latitude;
                this.longitude = this.listModifications[m].longitude;
                //id_monument = this.listModifications[m].monument.id;
                this.monumentOriginal = this.listModifications[m].monument.id;
                this.monumentOriginal += ", " + this.listModifications[m].monument.nom;
                this.monumentOriginal += " [" + this.listModifications[m].monument.type + "]<br>";
                this.monumentOriginal += this.listModifications[m].monument.description;
                this.monumentOriginalStyleCSS = this.getStyleCSSWithColor(this.listModifications[m].monument.type);
                this.idMonument = this.listModifications[m].monument.id;
                break;
            }
        }


    }

    refuseModification(){
        this.submitMsgs = [];

        this.submitTitle = "Succès";
        $("#suggestionForm").removeClass("was-validated");
        this.addMonument.deleteModification(this.idModif);
        this.removeFromList(this.idModif);
        //this.addMonument.validateMonument(this.idM, this.name, this.description, this.type, this.latitude, this.longitude);
        this.submitMsgs.push("La modification a bien été suprimée !");

        $("#validationDialog").modal('show');
    }

    getType(typeBDD){
        if (typeBDD == "MonumentHistorique"){
            return "historical";
        }
        if (typeBDD == "MaisonNatale"){
            return "house";
        }
        if (typeBDD == "Musee"){
            return "museum";
        }
        if (typeBDD == "Sculture"){
            return "sculpture";
        }
        if (typeBDD == "LieuxDeCulte"){
            return "religious";
        }
        if (typeBDD == "Murale"){
            return "mural";
        }
        if (typeBDD == "Anecdote"){
            return "anecdote";
        }
        return "";
      }
}
