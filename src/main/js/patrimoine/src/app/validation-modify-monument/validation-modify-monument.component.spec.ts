import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationModifyMonumentComponent } from './validation-modify-monument.component';

describe('ValidationModifyMonumentComponent', () => {
  let component: ValidationModifyMonumentComponent;
  let fixture: ComponentFixture<ValidationModifyMonumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationModifyMonumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationModifyMonumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
