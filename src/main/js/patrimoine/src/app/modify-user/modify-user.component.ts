import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Utilisateur } from '../utilisateur';
import { UserService } from '../user.service';

import * as bcrypt from 'bcryptjs';
declare var $ : any;

const MIN_PASS_LEN = 6;
const MIN_NAME_LEN = 3;


@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.css']
})
export class ModifyUserComponent implements OnInit {
   title = "Modification d'un utilisateur";
   submitTitle="Succès";
   submitMsgs=[];
   mail: string;
   role : String;
   user : Utilisateur;
   oSaisie : string;
   valid = false;
   userRole = [
           {id: 'Touriste', name: 'Touriste'},
           {id: 'Moderateur', name: 'Modérateur'},
           {id: 'Administrateur', name: 'Administrateur'},
   ];

  constructor(
          private userService : UserService,
          private route : ActivatedRoute,
          private router : Router
     ) {
     }

  ngOnInit() {
  }

    onSubmit() {

    // On retire tous les messages d'informations liés à d'éventuelles précèdentes soumissions.
          this.submitMsgs = [];
          let error = false;

          if (this.user.nom == "" || this.user.prenom == "" || this.user.mail=="" || this.user.adresse=="") {
              error = true;
              this.submitMsgs.push("Tous les champs doivent être remplis.");
          }
          if (this.user.nom.length < MIN_NAME_LEN) {
              error = true;
              this.submitMsgs.push("Le nom doit être composé d'au moins " + MIN_NAME_LEN + " caractères.");
          }
          if (this.user.prenom.length < MIN_NAME_LEN) {
              error = true;
              this.submitMsgs.push("Le prénom doit être composé d'au moins " + MIN_NAME_LEN + " caractères.");
          }
            if (error) {
                this.submitTitle = "Erreur";
                $("#modifyAdded").addClass("was-validated");
            } else {
                this.submitTitle = "Succès";
                $("#modifyUserForm").removeClass("was-validated");
                this.userService.modifyUser(this.user.id_user,this.user.nom, this.user.prenom, this.user.mail, this.user.adresse, this.role);
                this.submitMsgs.push("La modification a été effectuée");
            }
            $("#modifyAdded").modal('show');
         }
    search(){
        this.getUser();
    }
   getUser() {
      this.userService.getUserByMail(this.oSaisie).subscribe(user => {
          this.user = user;
          if(this.user != null){
                this.valid = true;
                this.role = this.user.role;
                document.getElementById("RechercheForm").style.display = "none";
                document.getElementById("containerModifyUser").style.display = "block";
          }else{
                document.getElementById("errorSearch").style.display = "block";
          }
      });
   }


}
