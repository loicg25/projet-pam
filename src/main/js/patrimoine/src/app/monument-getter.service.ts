import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Monument } from './monument';

@Injectable({
  providedIn: 'root'
})
export class MonumentGetterService {

  queryOption = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*'
  })};

  constructor(
    private http: HttpClient
  ) { }
  
  getMonument(id: number): Observable<Monument> {
    return this.http.get<Monument>("http://localhost:8080/monument/find/" + id, this.queryOption);
  }

  getMonumentStr(id: string): Observable<Monument> {
      return this.http.get<Monument>("http://localhost:8080/monument/find/" + id, this.queryOption);
    }

  getMonuments(): Observable<Monument[]> {
    return this.http.get<Monument[]>("http://localhost:8080/monument/findall", this.queryOption);
  }
  
  getMonumentsByType(type: string): Observable<Monument[]> {
    return this.http.get<Monument[]>("http://localhost:8080/monument/type/" + type, this.queryOption);
  }
}
