export interface Monument {
    id: number;
    nom: String;
    type: String;
    description: String;
    latitude: String;
    longitude: String
    estValide: boolean;
}
