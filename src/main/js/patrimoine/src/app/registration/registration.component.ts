import { Component, OnInit } from '@angular/core';

import { RegistrationService } from '../registration.service'

import * as bcrypt from 'bcryptjs';
declare var $ : any;

const MIN_PASS_LEN = 6;
const MIN_NAME_LEN = 3;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    title="S'inscrire";

    submitTitle="Succès";
    submitMsgs=[];

    name="";
    firstName="";
    email="";
    numero="";
    address="";
    cp="";
    city="";

    passW1="";
    passW2="";

  constructor(
    private registrationService : RegistrationService
   ) { }

  ngOnInit() {
  }

  static validPasswordFormat(pass) {
      return pass.length >= MIN_PASS_LEN && pass.match(/[A-Z]/g) && pass.match(/[0-9]/g);
  }

  static validEmailFormat(email) {
      return email.match(/[^@]+@[^@]+\.[^@]+/);
  }

  onSubmit() {
      // On retire tous les messages d'informations liés à d'éventuelles précèdentes soumissions.
      this.submitMsgs = [];
      let error = false;

      if (this.name == "" || this.firstName == "" || this.email=="" || this.numero=="" || this.address=="" || this.cp=="" || this.city=="" || this.passW1=="" || this.passW2=="") {
          error = true;
          this.submitMsgs.push("Tous les champs doivent être remplis.");
      }
      if (this.name.length < MIN_NAME_LEN) {
          error = true;
          this.submitMsgs.push("Le nom doit être composé d'au moins " + MIN_NAME_LEN + " caractères.");
      }
      if (this.firstName.length < MIN_NAME_LEN) {
          error = true;
          this.submitMsgs.push("Le prénom doit être composé d'au moins " + MIN_NAME_LEN + " caractères.");
      }
      if (!this.cp.match(/[0-9]{5}/g)){
          error = true;
          this.submitMsgs.push("Le code postal doit être un nombre (de 5 chiffres).");
      }
      if (!RegistrationComponent.validPasswordFormat(this.passW1)) {
          error = true;
          this.submitMsgs.push("Le mot de passe doit être composé d'au moins " + MIN_PASS_LEN + " caractères, contenir au moins une majuscule et au moins un chiffre.")
      }
      if (!RegistrationComponent.validEmailFormat(this.email)) {
          error = true;
          this.submitMsgs.push("L'adresse e-mail n'est pas valide.")
      }
      if (this.passW1 != this.passW2) {
          error = true;
          this.submitMsgs.push("Les mots de passe doivent être identiques.");
      }

        // Vérif que l'utilisateur n'existe déjà pas
        this.registrationService.userAlreadyExist(this.email).subscribe(used => {
            if (used){
                error = true;
                this.submitMsgs.push("L'adresse email est déjà utilisée.");
            }

              if (error) {
                  this.submitTitle = "Erreur";
                  $("#registrationForm").addClass("was-validated");
              } else {
                  this.submitTitle = "Succès";
                  $("#registrationForm").removeClass("was-validated");
                  this.submitMsgs.push("L'inscription a bien été réalisée.");

                  const salt = bcrypt.genSaltSync(10);
                  const hash = bcrypt.hashSync(this.passW1, salt);

                  let postalAddress = this.numero + " " + this.address + " " + this.cp + " " + this.city;

                  this.registrationService.registration(this.name, this.firstName, this.email, postalAddress, hash);
                  this.name="";
                  this.firstName="";
                  this.email="";
                  this.numero="";
                  this.address="";
                  this.cp="";
                  this.city="";
                  this.passW1="";
                  this.passW2="";
              }
        });
      $("#myModal").modal('show');
  }
}
