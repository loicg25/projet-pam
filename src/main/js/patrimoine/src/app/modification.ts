import { Monument } from './monument';

export interface Modification {
    id_modification: number;
    monument: Monument;
    nom: String;
    type: String;
    description: String;
    latitude: String;
    longitude: String;
}
