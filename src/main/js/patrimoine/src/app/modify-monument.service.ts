import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModifyMonumentService {
queryOption = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin':'*'
  })};
   constructor(private http: HttpClient) { }

  modifyMonument(id: number, nom: String, description: String, type: String, latitude: String, longitude: String){
      this.http.get("http://localhost:8080/monument/addmodification?id="+id+"&nom="+nom+"&description="+description+"&type="+type+"&latitude="+latitude+"&longitude="+longitude).subscribe();
    }

}
