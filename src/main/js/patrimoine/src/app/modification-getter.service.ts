import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Modification } from './modification';

@Injectable({
  providedIn: 'root'
})
export class ModificationGetterService {
queryOption = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin':'*'
  })};

  constructor(private http: HttpClient) {}

  getModifications(): Observable<Modification[]>{
    return this.http.get<Modification[]>("http://localhost:8080/monument/getAllModifications", this.queryOption);
  }
}
