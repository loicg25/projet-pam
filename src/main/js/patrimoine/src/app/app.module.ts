import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonumentViewerComponent } from './monument-viewer/monument-viewer.component';

import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './map/map.component';
import { SuggestionFormComponent } from './suggestion-form/suggestion-form.component';

import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MatFormFieldModule } from '@angular/material/form-field';
import { RegistrationComponent } from './registration/registration.component';
import { ConnectionComponent } from './connection/connection.component';
import { ValidationComponent } from './validation/validation.component';
import { ModifyMonumentComponent } from './modify-monument/modify-monument.component';
import { ValidationModifyMonumentComponent } from './validation-modify-monument/validation-modify-monument.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';

@NgModule({
  declarations: [
    AppComponent,
    MonumentViewerComponent,
    MapComponent,
    SuggestionFormComponent,
    RegistrationComponent,
    ConnectionComponent,
    ValidationComponent,
    ModifyMonumentComponent,
    ValidationModifyMonumentComponent,
    ModifyUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
