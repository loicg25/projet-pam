import { TestBed } from '@angular/core/testing';

import { AddMonumentService } from './add-monument.service';

describe('AddMonumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddMonumentService = TestBed.get(AddMonumentService);
    expect(service).toBeTruthy();
  });
});
