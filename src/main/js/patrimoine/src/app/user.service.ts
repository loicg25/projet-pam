import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Utilisateur } from './utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  queryOption = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin':'*'
    })};

    constructor(
      private http: HttpClient
    ) { }

    getUserByMail(mail: string): Observable<Utilisateur> {
        console.log(mail);
      return this.http.get<Utilisateur>("http://localhost:8080/user/find?mail=" + mail, this.queryOption);
    }

    modifyUser(id: number, nom: String, prenom: String, mail: String, adresse: String, role: String){
          this.http.get("http://localhost:8080/user/modification?nom="+nom+"&prenom="+prenom+"&mail="+mail+"&adresse="+adresse+"&role="+role).subscribe();
        }

    getUserById(id: number) : Observable<Utilisateur>{
        return this.http.get<Utilisateur>("http://localhost:8080/user/findById?id=" + id, this.queryOption);
    }

}
