import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MonumentGetterService } from '../monument-getter.service';

import { AddMonumentService } from '../add-monument.service';

import { EventManager } from '@angular/platform-browser';

import { ConnectionService } from '../connection.service';

import { UserService } from '../user.service';
import {Monument} from "../monument";

declare var $ : any;

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {
    title="Validation";
    submitTitle="";
    submitMsgs=[];

    idM : number;

    listMonuments : Monument[] = [];

    isAdmin = false;
    role = "";
    id_userCo : number;

    monumentToValid = false;

    isVisibleDivListeMonument = true;
    isVisibleFormValidation = false;

    coordUser;

    name : String = "";
    description : String ="";
    type : String = "";
    latitude : String = "";
    longitude : String = "";
    monumentCategory = [
            {id: 'historical', name: 'Monument Historique'},
            {id: 'house', name: 'Maisons Natales'},
            {id: 'museum', name: 'Musées'},
            {id: 'sculpture', name: 'Sculptures'},
            {id: 'religious', name: 'Lieux De Culte'},
            {id: 'anecdote', name: 'Murales'},
            {id: 'mural', name: 'Anecdotes'},
    ];

  constructor(
    private monumentGetter : MonumentGetterService,
    private addMonument : AddMonumentService,
    private router : Router,
    private eventManager: EventManager,
    private connectionService : ConnectionService,
    private userService : UserService,
  ) {

  }

  ngOnInit() {

    let connecte = this.connectionService.isConnected();
    if (connecte){
        this.role = this.connectionService.getRole();
        this.id_userCo = this.connectionService.getId();

        if (this.role == "Administrateur" || this.role == "Moderateur"){
            this.isAdmin = true;
            /*GET COORDONNEES OF USER*/
            this.userService.getUserById(this.id_userCo).subscribe(user => {
                $.get("https://nominatim.openstreetmap.org/search?format=json&q="+user.adresse, coordUser => { this.coordUser = coordUser; this.getMonuments() });
            });
        } else {
            this.accesInterdit();
        }
    } else {
        this.accesInterdit();
    }
    $("#form-validation").modal("none");
  }

  accesInterdit() {
        this.submitTitle = "Accès interdit";
        this.submitMsgs.push("Vous ne pouvez pas accèder à cette page !");
        $("#validationDialog").modal('show');
  }

  closeFct(){
    if (!this.isAdmin){
        this.router.navigate(["/"]); //maybe a remodifier
    } else if (!this.monumentToValid){
        this.router.navigate(["/"]);
    } else {
        if (this.submitTitle == "Succès") {
            this.isVisibleDivListeMonument = true;
            this.isVisibleFormValidation = false;
        }
    }
  }

  onSubmit(){
    this.submitMsgs = [];
    let error = false;
    if (this.name == "" || this.description == "" || this.latitude == "" || this.longitude == "" || this.type == ""){
        error = true;
        this.submitMsgs.push("Tout les champs doivent être remplis");
    }
    if (error) {
        this.submitTitle = "Erreur";
        $("#suggestionForm").addClass("was-validated");
    } else {
        this.submitTitle = "Succès";
        $("#suggestionForm").removeClass("was-validated");
        this.addMonument.validateMonument(this.idM, this.name, this.description, this.type, this.latitude, this.longitude);
        this.submitMsgs.push("Le monument a bien été validé");
        this.removeFromList(this.idM);
    }
    $("#validationDialog").modal('show');
  }

  getMonuments() {
    this.monumentGetter.getMonuments().subscribe(monuments => {
        let count = 0;
        for (let m in monuments){
            //console.log(monuments[m]);
            if (!monuments[m].estValide){
                let distance = this.getDistance(monuments[m].latitude, monuments[m].longitude, this.coordUser[0].lat, this.coordUser[0].lon);
                //console.log("Distance entre monument ", monuments[m].id, " et adresse : ", distance, " km");
                if ((this.role == "Moderateur" && distance <= 50) || this.role == "Administrateur"){
                    let mon = monuments[m];
                    mon["styleCSS"] = this.getStyleCSSWithColor(monuments[m].type);
                    this.listMonuments.push(mon);
                    count++;
                }
            }
        }
        if (count == 0){
            this.submitTitle = "Info";
            this.submitMsgs.push("Il n'y a pas de monument à valider");
            $("#validationDialog").modal('show');
        } else {
            this.monumentToValid = true;
        }
    });
  }

  degres2radian(x){
    return x * (Math.PI/180);
  }

  getDistance(lat1, lon1, lat2, lon2){
    //console.log("DISTANCE : ", lat1, ", ", lon1, " /// ", lat2, ", ", lon2);
    let earth_radius = 6378.137;
    let dLat = this.degres2radian(lat2-lat1);
    let dLon = this.degres2radian(lon2-lon1);
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(this.degres2radian(lat1)) * Math.cos(this.degres2radian(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = earth_radius * c;
    return d;
  }

  getStyleCSSWithColor(typeBDD){
    if (typeBDD == "MonumentHistorique"){
            return 'solid 2px #ffbf00';
        }
        if (typeBDD == "MaisonNatale"){
            return 'solid 2px #0362fc';
        }
        if (typeBDD == "Musee"){
            return 'solid 2px #03c200';
        }
        if (typeBDD == "Sculture"){
            return 'solid 2px #000000';
        }
        if (typeBDD == "LieuxDeCulte"){
            return 'solid 2px #0d00ff';
        }
        if (typeBDD == "Murale"){
            return 'solid 2px #00ff73';
        }
        if (typeBDD == "Anecdote"){
            return 'solid 2px #f200ff';
        }
        return '';
      }


    choiceMonument(id){
        this.affichageFormulaire(id);
    }

    removeFromList(id) {
      let index = 0;
      for (let i = 0; i < this.listMonuments.length; ++i) {
          if (this.listMonuments[i].id == id) {
              index = i;
              break;
          }
      }
      this.listMonuments.splice(index, 1);
    }

    affichageFormulaire(id){
        this.isVisibleDivListeMonument = false;
        this.isVisibleFormValidation = true;
        for (let m in this.listMonuments){
            if (this.listMonuments[m].id == id){
                this.idM = this.listMonuments[m].id;
                this.name = this.listMonuments[m].nom;
                this.description = this.listMonuments[m].description;
                this.type = this.getType(this.listMonuments[m].type);
                this.latitude = this.listMonuments[m].latitude;
                this.longitude = this.listMonuments[m].longitude;
                break;
            }
        }

    }

    refuseMonument(){
        this.submitMsgs = [];

        this.submitTitle = "Succès";
        $("#suggestionForm").removeClass("was-validated");
        this.addMonument.deleteMonument(this.idM);
        this.submitMsgs.push("Ce monument a bien été supprimé !");
        this.removeFromList(this.idM);

        $("#validationDialog").modal('show');
    }

  getType(typeBDD){
    if (typeBDD == "MonumentHistorique"){
        return "historical";
    }
    if (typeBDD == "MaisonNatale"){
        return "house";
    }
    if (typeBDD == "Musee"){
        return "museum";
    }
    if (typeBDD == "Sculture"){
        return "sculpture";
    }
    if (typeBDD == "LieuxDeCulte"){
        return "religious";
    }
    if (typeBDD == "Murale"){
        return "mural";
    }
    if (typeBDD == "Anecdote"){
        return "anecdote";
    }
    return "";
  }

}
