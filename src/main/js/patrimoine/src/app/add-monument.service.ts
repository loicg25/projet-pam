import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddMonumentService {

  constructor(private http: HttpClient) { }

  addMonument(nom: String, description: String, type: String, latitude: String, longitude: String){
    this.http.get("http://localhost:8080/monument/addmonument?nom="+nom+"&description="+description+"&type="+type+"&latitude="+latitude+"&longitude="+longitude).subscribe();
  }

  validateMonument(id: number, nom: String, description: String, type: String, latitude: String, longitude: String){
    this.http.get("http://localhost:8080/monument/validateMonument?id="+id+"&nom="+nom+"&description="+description+"&type="+type+"&latitude="+latitude+"&longitude="+longitude).subscribe();
  }

  modifyMonument(idMonument: number, nom: String, description: String, type: String, latitude: String, longitude: String, idModification: number){
    this.http.get("http://localhost:8080/monument/modifyMonument?idMonument="+idMonument+"&nom="+nom+"&description="+description+"&type="+type+"&latitude="+latitude+"&longitude="+longitude+"&idModification="+idModification).subscribe();
  }

  deleteModification(idModification: number){
    this.http.get("http://localhost:8080/monument/deleteModification?id="+idModification).subscribe();
  }

  deleteMonument(idMonument: number){
    this.http.get("http://localhost:8080/monument/deleteMonument?id="+idMonument).subscribe();
  }
}
