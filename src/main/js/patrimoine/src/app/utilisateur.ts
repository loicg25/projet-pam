export interface Utilisateur {
    id_user: number;
    nom: String;
    prenom: String;
    adresse: String;
    mail: String;
    mdp: String;
    role: String;
}
