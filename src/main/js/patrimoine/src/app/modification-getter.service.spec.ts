import { TestBed } from '@angular/core/testing';

import { ModificationGetterService } from './modification-getter.service';

describe('ModificationGetterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModificationGetterService = TestBed.get(ModificationGetterService);
    expect(service).toBeTruthy();
  });
});
