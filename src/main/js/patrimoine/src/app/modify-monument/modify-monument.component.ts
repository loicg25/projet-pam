import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Monument } from '../monument';
import { MonumentGetterService } from '../monument-getter.service';
import { ModifyMonumentService } from '../modify-monument.service';
import {ConnectionService} from "../connection.service";
declare var $ : any;

const MIN_NAME_LEN = 3;
const MIN_DESC_LEN = 5;

@Component({
  selector: 'app-modify-monument',
  templateUrl: './modify-monument.component.html',
  styleUrls: ['./modify-monument.component.css']
})
export class ModifyMonumentComponent implements OnInit {
    title = "Modification d'un monument";
    submitTitle="Succès";
    submitMsgs=[];
    type = "";
    id: string;
    monument : Monument;
    monumentCategory = [
            {id: 'historical', name: 'Monument Historique'},
            {id: 'house', name: 'Maisons Natales'},
            {id: 'museum', name: 'Musées'},
            {id: 'sculpture', name: 'Sculptures'},
            {id: 'religious', name: 'Lieux De Culte'},
            {id: 'mural', name: 'Murales'},
            {id: 'anecdote', name: 'Anecdotes'},
    ];


  constructor(
        private monumentGetter : MonumentGetterService,
        private modifyMonumentService : ModifyMonumentService,
        private connectionService : ConnectionService,
        private route : ActivatedRoute,
        private router : Router
   ) {
   }


  ngOnInit() {
      this.id = this.route.snapshot.queryParamMap.get("id");
      this.getMonument();
      let connecte = this.connectionService.isConnected();
      if (!connecte) {
          this.accesInterdit()
      }
  }

    accesInterdit() {
        this.submitTitle = "Accès interdit";
        this.submitMsgs.push("Vous ne pouvez pas accèder à cette page !");
        console.log(this.submitTitle);
        $("#modifyAdded").modal('show');
    }

    closeFct(){
        if (!this.connectionService.isConnected()) {
            this.router.navigate(["/"]);
        }
    }

  onSubmit() {
          // On retire tous les messages d'informations liés à d'éventuelles précèdentes soumissions.
          this.submitMsgs = [];
          let error = false;

          if (this.monument.nom.length < MIN_NAME_LEN) {
              error = true;
              this.submitMsgs.push("Le nom doit être composée d'au moins " + MIN_NAME_LEN + " caractères.");
          }
          if (this.monument.description.length < MIN_DESC_LEN) {
              error = true;
              this.submitMsgs.push( "La description doit être composée d'au moins " + MIN_DESC_LEN + " caractères.");
          }
          if (this.type == "") {
              error = true;
              this.submitMsgs.push("Vous devez renseigner une catégorie.");
          }

          if (error) {
              this.submitTitle = "Erreur";
              $("#modifyForm").addClass("was-validated");
          } else {
              this.submitTitle = "Succès";
              $("#suggestionForm").removeClass("was-validated");
              this.modifyMonumentService.modifyMonument(this.monument.id,this.monument.nom, this.monument.description, this.type, this.monument.latitude, this.monument.longitude);
              this.submitMsgs.push("La modification a été envoyée");
          }
          $("#modifyAdded").modal('show');
       }

  getMonument() {
    this.monumentGetter.getMonumentStr(this.id).subscribe(monument => {this.monument = monument; this.type = this.getType(this.monument.type);});
  }

  getType(typeBDD){
    if (typeBDD == "MonumentHistorique"){
        return "historical";
    }
    if (typeBDD == "MaisonNatale"){
        return "house";
    }
    if (typeBDD == "Musee"){
        return "museum";
    }
    if (typeBDD == "Sculture"){
        return "sculpture";
    }
    if (typeBDD == "LieuxDeCulte"){
        return "religious";
    }
    if (typeBDD == "Murale"){
        return "mural";
    }
    if (typeBDD == "Anecdote"){
        return "anecdote";
    }
    return "";
  }
}

