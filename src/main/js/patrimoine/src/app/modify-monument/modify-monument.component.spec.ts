import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyMonumentComponent } from './modify-monument.component';

describe('ModifyMonumentComponent', () => {
  let component: ModifyMonumentComponent;
  let fixture: ComponentFixture<ModifyMonumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyMonumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyMonumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
