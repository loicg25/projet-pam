import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ConnectionService } from "./connection.service";

declare var ol: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Patrimoine';
    currentRoute: string;

    constructor(public login: ConnectionService, private router: Router) {
        router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe(event =>
        { this.currentRoute = event.url; });
    }
}
