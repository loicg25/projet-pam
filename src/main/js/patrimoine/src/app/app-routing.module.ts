import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MonumentViewerComponent } from './monument-viewer/monument-viewer.component';
import { RegistrationComponent } from './registration/registration.component';
import { ConnectionComponent } from './connection/connection.component';
import { MapComponent } from './map/map.component';
import { SuggestionFormComponent } from './suggestion-form/suggestion-form.component';
import { ValidationComponent } from './validation/validation.component';
import { ModifyMonumentComponent } from './modify-monument/modify-monument.component';
import { ValidationModifyMonumentComponent } from './validation-modify-monument/validation-modify-monument.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';

const routes: Routes = [
    { path: '', component: MapComponent},
    { path: 'monument-viewer', component: MonumentViewerComponent},
    { path: 'connection', component: ConnectionComponent},
    { path: 'registration', component: RegistrationComponent},
    { path: 'suggestion-form', component: SuggestionFormComponent },
    { path: 'validation', component: ValidationComponent },
    { path: 'modify', component: ModifyMonumentComponent },
    { path: 'validation-modify', component : ValidationModifyMonumentComponent},
    { path: 'modify-user', component : ModifyUserComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
