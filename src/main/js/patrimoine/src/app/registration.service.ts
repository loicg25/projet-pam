import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

queryOption = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin':'*'
  })};

  constructor(private http: HttpClient) { }

  registration(name: String, firstName: String, email: String, address: String, password: String){
    //console.log("dans le service");
    this.http.get("http://localhost:8080/user/addUser?nom="+name+"&prenom="+firstName+"&mail="+email+"&adresse="+address+"&mdp="+password).subscribe();
  }

  userAlreadyExist(email: String): Observable<String>{
    return this.http.get<String>("http://localhost:8080/user/userAlreadyExist?mail="+email, this.queryOption);
  }
}
