import { TestBed } from '@angular/core/testing';

import { ModifyMonumentService } from './modify-monument.service';

describe('ModifyMonumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModifyMonumentService = TestBed.get(ModifyMonumentService);
    expect(service).toBeTruthy();
  });
});
