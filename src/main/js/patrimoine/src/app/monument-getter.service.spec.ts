import { TestBed } from '@angular/core/testing';

import { MonumentGetterService } from './monument-getter.service';

describe('MonumentGetterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonumentGetterService = TestBed.get(MonumentGetterService);
    expect(service).toBeTruthy();
  });
});
