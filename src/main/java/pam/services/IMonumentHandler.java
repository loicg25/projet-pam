package pam.services;

import pam.model.Monument;
import pam.model.TypeMonument;
import pam.repositories.MonumentRepository;

public interface IMonumentHandler {
    Monument findOneMonument(long id);
    Iterable<Monument> findAllMonuments();
    Iterable<Monument> findMonumentsByType(TypeMonument monument);
}
