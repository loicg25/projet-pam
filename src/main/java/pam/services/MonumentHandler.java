package pam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pam.model.Monument;
import pam.model.TypeMonument;
import pam.repositories.MonumentRepository;

@Service
public class MonumentHandler implements IMonumentHandler {
    @Autowired
    MonumentRepository monumentRepository;


    @Override
    public Monument findOneMonument(long id) {
        return monumentRepository.findById(id);
    }

    @Override
    public Iterable<Monument> findAllMonuments() {
        return monumentRepository.findAll();
    }

    @Override
    public Iterable<Monument> findMonumentsByType(TypeMonument type) {
        return monumentRepository.findByType(type);
    }

    public void addMonument(String nom, String description, String type, String latitude, String longitude){
        TypeMonument t;

        switch (type){
            case "historical":
                t = TypeMonument.MonumentHistorique;
                break;
            case "house":
                t = TypeMonument.MaisonNatale;
                break;
            case "museum":
                t = TypeMonument.Musee;
                break;
            case "sculpture":
                t = TypeMonument.Sculture;
                break;
            case "religious":
                t = TypeMonument.LieuxDeCulte;
                break;
            case "anecdote":
                t = TypeMonument.Anecdote;
                break;
            case "mural":
                t = TypeMonument.Murale;
                break;
            default:
                t = TypeMonument.MonumentHistorique;
        }

        Monument m = new Monument(nom, description, t, latitude, longitude);
        monumentRepository.save(m);
    }

    public void validateMonument(long id, String nom, String description, String type, String latitude, String longitude) {
        TypeMonument t;
        switch (type){
            case "historical":
                t = TypeMonument.MonumentHistorique;
                break;
            case "house":
                t = TypeMonument.MaisonNatale;
                break;
            case "museum":
                t = TypeMonument.Musee;
                break;
            case "sculpture":
                t = TypeMonument.Sculture;
                break;
            case "religious":
                t = TypeMonument.LieuxDeCulte;
                break;
            case "anecdote":
                t = TypeMonument.Anecdote;
                break;
            case "mural":
                t = TypeMonument.Murale;
                break;
            default:
                t = TypeMonument.MonumentHistorique;
        }

        Monument m = monumentRepository.findById(id);
        if (m.getNom() != nom){
            m.setNom(nom);
        }
        if (m.getDescription() != description){
            m.setDescription(description);
        }
        if (m.getType() != t){
            m.setType(t);
        }
        if (m.getLatitude() != latitude){
            m.setLatitude(latitude);
        }
        if (m.getLongitude() != longitude){
            m.setLongitude(longitude);
        }
        m.setEstValide(true);
        monumentRepository.save(m);
    }

    public void modifyMonument(long id, String nom, String description, String type, String latitude, String longitude){
        TypeMonument t;
        switch (type){
            case "historical":
                t = TypeMonument.MonumentHistorique;
                break;
            case "house":
                t = TypeMonument.MaisonNatale;
                break;
            case "museum":
                t = TypeMonument.Musee;
                break;
            case "sculpture":
                t = TypeMonument.Sculture;
                break;
            case "religious":
                t = TypeMonument.LieuxDeCulte;
                break;
            case "anecdote":
                t = TypeMonument.Anecdote;
                break;
            case "mural":
                t = TypeMonument.Murale;
                break;
            default:
                t = TypeMonument.MonumentHistorique;
        }
        Monument m = monumentRepository.findById(id);
        if (m.getNom() != nom){
            m.setNom(nom);
        }
        if (m.getDescription() != description){
            m.setDescription(description);
        }
        if (m.getType() != t){
            m.setType(t);
        }
        if (m.getLatitude() != latitude){
            m.setLatitude(latitude);
        }
        if (m.getLongitude() != longitude){
            m.setLongitude(longitude);
        }
        m.setEstValide(true);
        monumentRepository.save(m);
    }

    public void deleteMonument(long id){
        monumentRepository.delete(id);
    }
}
