package pam.services;

import pam.model.Utilisateur;

public interface IUtilisateurHandler {
    Utilisateur findOneUtilisateur(String mail, String mdp);
    Utilisateur findOneUtilisateurWithMail(String mail);
    Iterable<Utilisateur> getAllUsers();
}
