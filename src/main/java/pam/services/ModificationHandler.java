package pam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pam.model.Modification;
import pam.model.Monument;
import pam.model.TypeMonument;
import pam.repositories.ModificationRepository;
import pam.repositories.MonumentRepository;

@Service
public class ModificationHandler implements IModificationHandler {
    @Autowired
    ModificationRepository modifRepository;
    @Autowired
    MonumentRepository monumentRepository;

    public void addModification(String id, String nom, String description, String type, String latitude, String longitude){
        TypeMonument t;
        switch (type){
            case "historical":
                t = TypeMonument.MonumentHistorique;
                break;
            case "house":
                t = TypeMonument.MaisonNatale;
                break;
            case "museum":
                t = TypeMonument.Musee;
                break;
            case "sculpture":
                t = TypeMonument.Sculture;
                break;
            case "religious":
                t = TypeMonument.LieuxDeCulte;
                break;
            case "anecdote":
                t = TypeMonument.Anecdote;
                break;
            case "mural":
                t = TypeMonument.Murale;
                break;
            default:
                t = TypeMonument.MonumentHistorique;
        }

        Monument monument = monumentRepository.findById(Integer.valueOf(id));
        Modification m = new Modification(monument,nom, description, t, latitude, longitude);
        modifRepository.save(m);
    }

    public Iterable<Modification> getAllModifications(){
        return modifRepository.findAll();
    }

    public void deleteModification(long id){
        modifRepository.delete(id);
    }


}
