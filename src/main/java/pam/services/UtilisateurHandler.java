package pam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pam.model.Monument;
import pam.model.Role;
import pam.model.Utilisateur;
import pam.repositories.UtilisateurRepository;

@Service
public class UtilisateurHandler implements IUtilisateurHandler {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Utilisateur findOneUtilisateur(String mail, String mdp) {
        return utilisateurRepository.findByMailAndMdp(mail, mdp);
    }

    @Override
    public Utilisateur findOneUtilisateurWithMail(String mail){
        return utilisateurRepository.findByMail(mail);
    }

    @Override
    public Iterable<Utilisateur> getAllUsers() {
        return utilisateurRepository.findAll();
    }

    public void addUser(String nom, String prenom, String mail, String adresse, String mdp) {
        Utilisateur u = new Utilisateur(nom, prenom, adresse, mail, mdp);
        utilisateurRepository.save(u);
    }

    public Utilisateur tryconnection(String mail, String mdp) {
       return utilisateurRepository.findByMailAndMdp(mail, mdp);
    }

    public void modification(String nom, String prenom, String mail, String adresse, String role) {
        Role r;
        switch (role){
            case "Touriste":
                r = Role.Touriste;
                break;
            case "Moderateur":
                r = Role.Moderateur;
                break;
            case "Administrateur":
                r = Role.Administrateur;
                break;
            default:
                r = Role.Touriste;
        }
        Utilisateur u = utilisateurRepository.findByMail(mail);
        if (u.getNom() != nom){
            u.setNom(nom);
        }
        if (u.getPrenom() != prenom){
            u.setPrenom(prenom);
        }
        if(u.getAdresse() != adresse){
            u.setAdresse(adresse);
        }
        if(u.getRole() != r){
            u.setRole(r);
        }
        utilisateurRepository.save(u);
    }

    public Utilisateur findOneUtilisateurWithId(long id) {
        return utilisateurRepository.findOne(id);
    }
}
