package pam.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pam.model.Monument;
import pam.model.TypeMonument;

@Repository
public interface MonumentRepository extends CrudRepository<Monument, Long> {
    Monument findById(long id);
    Iterable<Monument> findByType(TypeMonument type);
}
