package pam.repositories;

import org.springframework.data.repository.CrudRepository;
import pam.model.Utilisateur;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
    Utilisateur findByMailAndMdp(String mail, String mdp);
    Utilisateur findByMail(String mail);
}
