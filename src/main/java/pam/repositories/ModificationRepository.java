package pam.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pam.model.Modification;

@Repository
public interface ModificationRepository extends CrudRepository<Modification, Long> {

}
