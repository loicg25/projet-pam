package pam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id_user;

    String nom;
    String prenom;
    String adresse;
    String mail;
    String mdp;
    Role role;

    public Utilisateur(){}
    public Utilisateur(long id_user, String nom, String prenom, String adresse, String mail, String mdp, Role role) {
        this.id_user = id_user;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.mail = mail;
        this.mdp = mdp;
        this.role = role;
    }


    public Utilisateur(String nom, String prenom, String adresse, String mail, String mdp, Role role) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.mail = mail;
        this.mdp = mdp;
        this.role = role;
    }

    public Utilisateur(String nom, String prenom, String adresse, String mail, String mdp) {
        this(nom, prenom, adresse, mail, mdp, Role.Touriste);
    }

    public long getId_user() {
        return id_user;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getMail() {
        return mail;
    }

    public String getMdp() {
        return mdp;
    }

    public Role getRole() {
        return role;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
