package pam.model;

import javax.persistence.*;

@Entity
public class Modification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id_modification;

    @ManyToOne
    Monument monument;

    String nom;
    String description;
    TypeMonument type;
    String latitude;
    String longitude;

    public Modification(Monument monument, String nom, String description, TypeMonument type, String latitude, String longitude) {
        this.monument = monument;
        this.nom = nom;
        this.description = description;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Modification(){}

    public long getId_modification() {
        return id_modification;
    }

    public Monument getMonument() {
        return monument;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public TypeMonument getType() {
        return type;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setMonument(Monument monument) {
        this.monument = monument;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(TypeMonument type) {
        this.type = type;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
