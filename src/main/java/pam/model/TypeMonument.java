package pam.model;

public enum TypeMonument {
    MonumentHistorique,
    MaisonNatale,
    Musee,
    Sculture,
    LieuxDeCulte,
    Murale,
    Anecdote,
}
