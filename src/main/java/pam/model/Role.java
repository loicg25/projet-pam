package pam.model;

public enum Role {
    Touriste,
    Moderateur,
    Administrateur,
}
