package pam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Monument {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    String nom;
    String description;
    TypeMonument type;
    String latitude;
    String longitude;
    boolean estValide;

    public Monument(String nom, String description, TypeMonument type, String latitude, String longitude, boolean estValide) {
        this.nom = nom;
        this.description = description;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.estValide = estValide;
    }

    public Monument(String nom, String description, TypeMonument type, String latitude, String longitude) {
        this(nom, description, type, latitude, longitude, false);
    }

    public Monument() {}

    public long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public TypeMonument getType() {
        return type;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public boolean isEstValide() {
        return estValide;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(TypeMonument type) {
        this.type = type;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setEstValide(boolean estValide) {
        this.estValide = estValide;
    }
}
