package pam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pam.model.Modification;
import pam.model.Monument;
import pam.model.TypeMonument;
import pam.services.ModificationHandler;
import pam.services.MonumentHandler;

@RestController
@RequestMapping("monument")
public class MonumentController {
    @Autowired
    private MonumentHandler monumentHandler;
    @Autowired
    private ModificationHandler modificationHandler;

    @RequestMapping("find/{id}")
    @ResponseBody
    @CrossOrigin
    public Monument findOneMonument(@PathVariable("id") long id ) {
        return monumentHandler.findOneMonument(id);
    }

    @RequestMapping("findall")
    @ResponseBody
    @CrossOrigin
    public Iterable<Monument> findAllMonuments() {
        return monumentHandler.findAllMonuments();
    }

    @RequestMapping("addmonument")
    @ResponseBody
    @CrossOrigin
    public String addMonument (@RequestParam(value="nom", required = false) String nom, @RequestParam(value="description", required=false) String description, @RequestParam(value="type", required = false) String type, @RequestParam(value="latitude", required = false) String latitude, @RequestParam(value="longitude", required = false) String longitude, Model model){
        monumentHandler.addMonument(nom, description, type, latitude, longitude);
        return "true";
    }

    @RequestMapping("addmodification")
    @ResponseBody
    @CrossOrigin
    public String addmodification(@RequestParam(value="id", required = false) String id,@RequestParam(value="nom", required = false) String nom, @RequestParam(value="description", required=false) String description, @RequestParam(value="type", required = false) String type, @RequestParam(value="latitude", required = false) String latitude, @RequestParam(value="longitude", required = false) String longitude, Model model){
        modificationHandler.addModification(id, nom, description, type, latitude, longitude);
        return "true";
    }

    @RequestMapping("type/{type}")
    @ResponseBody
    @CrossOrigin
    public Iterable<Monument> findMonumentsByType(@PathVariable("type") String type) {
        if (type.equals("historical")) {
            return monumentHandler.findMonumentsByType(TypeMonument.MonumentHistorique);
        }
        if (type.equals("house")) {
            return monumentHandler.findMonumentsByType(TypeMonument.MaisonNatale);
        }
        if (type.equals("museum")) {
            return monumentHandler.findMonumentsByType(TypeMonument.Musee);
        }
        if (type.equals("sculpture")) {
            return monumentHandler.findMonumentsByType(TypeMonument.Sculture    );
        }
        if (type.equals("religious")) {
            return monumentHandler.findMonumentsByType(TypeMonument.LieuxDeCulte);
        }
        if (type.equals("anecdote")) {
            return monumentHandler.findMonumentsByType(TypeMonument.Anecdote);
        }
        if (type.equals("mural")) {
            return monumentHandler.findMonumentsByType(TypeMonument.Murale);
        }
        return null;
    }

    @RequestMapping("validateMonument")
    @ResponseBody
    @CrossOrigin
    public String validateMonument(@RequestParam(value="id", required=true) long id, @RequestParam(value="nom", required = false) String nom, @RequestParam(value="description", required=false) String description, @RequestParam(value="type", required = false) String type, @RequestParam(value="latitude", required = false) String latitude, @RequestParam(value="longitude", required = false) String longitude, Model model){
        monumentHandler.validateMonument(id, nom, description, type, latitude, longitude);
        return null;
    }

    @RequestMapping("getAllModifications")
    @ResponseBody
    @CrossOrigin
    public Iterable<Modification> getAllModifications(){
        return modificationHandler.getAllModifications();
    }

    @RequestMapping("modifyMonument")
    @ResponseBody
    @CrossOrigin
    public String modifyMonument(@RequestParam(value="idMonument", required = true) long idMonument, @RequestParam(value="nom", required = false) String nom, @RequestParam(value="description", required=false) String description, @RequestParam(value="type", required = false) String type, @RequestParam(value="latitude", required = false) String latitude, @RequestParam(value="longitude", required = false) String longitude, @RequestParam(value="idModification", required=true) long idModification, Model model) {
        monumentHandler.modifyMonument(idMonument, nom, description, type, latitude, longitude);
        modificationHandler.deleteModification(idModification);
        return null;
    }

    @RequestMapping("deleteModification")
    @ResponseBody
    @CrossOrigin
    public String deleteModification(@RequestParam(value="id", required=true) long id, Model model){
        modificationHandler.deleteModification(id);
        return null;
    }

    @RequestMapping("deleteMonument")
    @ResponseBody
    @CrossOrigin
    public String deleteMonument(@RequestParam(value="id", required=true) long id, Model model){
        monumentHandler.deleteMonument(id);
        return null;
    }
}
