package pam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import pam.model.Monument;
import pam.model.Utilisateur;
import pam.services.UtilisateurHandler;


@RestController
@RequestMapping("user")
public class UtilisateurController {
    @Autowired
    private UtilisateurHandler utilisateurHandler;

    @RequestMapping("find")
    @ResponseBody
    @CrossOrigin
    public Utilisateur findUtilisateur(@RequestParam(value="mail", required = false) String mail) {
        Utilisateur user = utilisateurHandler.findOneUtilisateurWithMail(mail);
        return utilisateurHandler.findOneUtilisateurWithMail(mail);
    }

    @RequestMapping("addUser")
    @ResponseBody
    @CrossOrigin
    public String addUser(@RequestParam(value="nom", required = false) String nom, @RequestParam(value="prenom", required = false) String prenom, @RequestParam(value="mail", required = false) String mail, @RequestParam(value="adresse", required = false) String adresse, @RequestParam(value="mdp", required = false) String mdp, Model model){
        utilisateurHandler.addUser(nom, prenom, mail, adresse, mdp);
        return "true";
    }

    @RequestMapping("modification")
    @ResponseBody
    @CrossOrigin
    public String modification(@RequestParam(value="nom", required = false) String nom, @RequestParam(value="prenom", required=false) String prenom, @RequestParam(value="mail", required = false) String mail, @RequestParam(value="adresse", required = false) String adresse, @RequestParam(value="role", required = false) String role, Model model){
        utilisateurHandler.modification(nom, prenom, mail, adresse, role);
        return "true";
    }


    @RequestMapping("tryconnection")
    @ResponseBody
    @CrossOrigin
    public Utilisateur tryconnection(@RequestParam(value="mail", required = true) String mail, Model model){
        System.out.println(mail);
        System.out.println(utilisateurHandler.findOneUtilisateurWithMail(mail));
        return utilisateurHandler.findOneUtilisateurWithMail(mail);
    }

    @RequestMapping("userAlreadyExist")
    @ResponseBody
    @CrossOrigin
    public String userAlreadyExist(@RequestParam(value="mail", required = true) String mail, Model model){
        Utilisateur u = utilisateurHandler.findOneUtilisateurWithMail(mail);
        if (u == null){
            return "false";
        }
        return "true";
    }

    @RequestMapping("findById")
    @ResponseBody
    @CrossOrigin
    public Utilisateur findUtilisateurById(@RequestParam(value="id", required=true) long id, Model model){
        return utilisateurHandler.findOneUtilisateurWithId(id);
    }
}
